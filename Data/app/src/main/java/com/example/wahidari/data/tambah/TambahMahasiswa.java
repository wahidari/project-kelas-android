package com.example.wahidari.data.tambah;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wahidari.data.DataHelper;
import com.example.wahidari.data.DataMahasiswa;
import com.example.wahidari.data.R;

public class TambahMahasiswa extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;
    EditText text1, text2, text3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_mahasiswa);

        // Initiation Button & TextView
        dbHelper = new DataHelper(this);
        text1 = findViewById(R.id.editText1);
        text2 = findViewById(R.id.editText2);
        text3 = findViewById(R.id.editText3);
        Button button1  =  findViewById(R.id.button1);
        Button button2  =  findViewById(R.id.button2);

        // Button Save Clicked
        button1. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                // Input Complete
                if (!text1.getText().toString().isEmpty() && !text2.getText().toString().isEmpty() && !text3.getText().toString().isEmpty()) {

                    // Try to Inserting New Row to Database
                    try {
                        SQLiteDatabase db = dbHelper.getWritableDatabase();
                        db.execSQL("insert into data_mahasiswa(nim, nama, alamat) values('" +
                                text1.getText().toString() + "','" +
                                text2.getText().toString() + "','" +
                                text3.getText().toString() + "')");
                        Toast.makeText(getApplicationContext(), "Data Mahasiswa Berhasil Di Simpan", Toast.LENGTH_LONG).show();
                    }

                    // Catch If Fail Inserting New Row to Database (ex : Duplicate PK)
                    catch (Exception ex) {
                        System.out.println("Exceptions " + ex);
                        Log.e("Note", "One row entered");
                        Toast.makeText(getApplicationContext(), "Data Mahasiswa Gagal Disimpan !", Toast.LENGTH_LONG).show();
                    }

                    // Refresh the Database
                    DataMahasiswa.datamahasiswa.RefreshList();
                    finish();
                }

                // Missing Input
                else {
                    Toast.makeText(getApplicationContext(), "Data Mahasiswa Tidak Boleh Kosong !", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Button Back Clicked
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }
}
