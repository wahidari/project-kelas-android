package com.example.wahidari.data;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.wahidari.data.tambah.TambahLab;

public class MainActivity extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbcenter;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initislize layout
        CardView btn_mahasiswa  =(CardView) findViewById(R.id.button_mahasiswa);
        CardView btn_dosen      =(CardView) findViewById(R.id.button_dosen);
        CardView btn_matakuliah =(CardView) findViewById(R.id.button_matakuliah);
        CardView btn_lab        =(CardView) findViewById(R.id.button_lab);

        //initialite layout onClick
        btn_mahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, DataMahasiswa.class);
                startActivity(intent);
            }
        });

        btn_dosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, DataDosen.class);
                startActivity(intent);
            }
        });

        btn_matakuliah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, DataMatakuliah.class);
                startActivity(intent);
            }
        });

        btn_lab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, DataLab.class);
                startActivity(intent);
            }
        });

        ma = this;
        dbcenter = new DataHelper(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.about) {
            Intent inte = new Intent(MainActivity.this, About.class);
            startActivity(inte);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
