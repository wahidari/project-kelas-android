package com.example.wahidari.data.lihat;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.wahidari.data.DataHelper;
import com.example.wahidari.data.R;

public class LihatDosen extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_dosen);

        // Initiation Button & TextView
        dbHelper = new DataHelper(this);
        TextView text1 =  findViewById(R.id.textView1);
        TextView text2 =  findViewById(R.id.textView2);
        TextView text3 =  findViewById(R.id.textView3);
        Button button = findViewById(R.id.button1);

        // Get Data From DB using Name
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM data_dosen WHERE nama = '" +
                getIntent().getStringExtra("nama") + "'",null);
        cursor.moveToFirst();

        // Set Data Value to TextView
        if (cursor.getCount()>0){
            cursor.moveToPosition(0);
            text1.setText(cursor.getString(0).toString());
            text2.setText(cursor.getString(1).toString());
            text3.setText(cursor.getString(2).toString());
        }

        // Back Button Clicked
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }
}
