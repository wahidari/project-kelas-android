package com.example.wahidari.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "data.db";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_MAHASISWA  = "create table data_mahasiswa(nim integer primary key, nama text null, alamat text null);";
    private static final String CREATE_TABLE_DOSEN      = "create table data_dosen(nip integer primary key, nama text null, alamat text null);";
    private static final String CREATE_TABLE_MATAKULIAH = "create table data_matakuliah(kode integer primary key, nama text null, sks integer null);";
    private static final String CREATE_TABLE_LAB        = "create table data_lab(kode integer primary key, nama text null);";

    private static final String INSERT_TABLE_MAHASISWA  = "INSERT INTO data_mahasiswa (nim, nama, alamat) VALUES ('150411100121', 'Wahid Arinanto N', 'Bangkalan');";
    private static final String INSERT_TABLE_DOSEN      = "INSERT INTO data_dosen (nip, nama, alamat) VALUES ('1000', 'Dosen A', 'Surabaya');";
    private static final String INSERT_TABLE_MATAKULIAH = "INSERT INTO data_matakuliah (kode, nama, sks) VALUES ('100', 'Pemrograman Perangkat Bergerak', '4');";
    private static final String INSERT_TABLE_LAB        = "INSERT INTO data_lab (kode, nama) VALUES ('100', 'Common Computing');";

    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        // Create Table
        db.execSQL(CREATE_TABLE_MAHASISWA);
        db.execSQL(CREATE_TABLE_DOSEN);
        db.execSQL(CREATE_TABLE_MATAKULIAH);
        db.execSQL(CREATE_TABLE_LAB);

        // Insert To Table
        db.execSQL(INSERT_TABLE_MAHASISWA);
        db.execSQL(INSERT_TABLE_DOSEN);
        db.execSQL(INSERT_TABLE_MATAKULIAH);
        db.execSQL(INSERT_TABLE_LAB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub
    }
}
