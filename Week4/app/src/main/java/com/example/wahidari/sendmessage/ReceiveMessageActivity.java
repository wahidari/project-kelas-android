package com.example.wahidari.sendmessage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ReceiveMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_message);

        TextView tvData1 = (TextView) findViewById(R.id.textView3);
        tvData1.setText(getIntent().getStringExtra("data1"));
    }
}
