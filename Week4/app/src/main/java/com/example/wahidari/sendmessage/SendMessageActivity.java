package com.example.wahidari.sendmessage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SendMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

    }

    public void SendIntent(View view) {
        final EditText pesan = (EditText) findViewById(R.id.editText2);
        if (!pesan.getText().toString().isEmpty()) {
            Intent intent = new Intent(this, ReceiveMessageActivity.class);
            intent.putExtra("data1", pesan.getText().toString());
            startActivity(intent);
        } else {
            Toast.makeText(this, "Pesan tidak boleh kosong!",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void SendSms(View view) {
        final EditText pesan = (EditText) findViewById(R.id.editText2);
        if (!pesan.getText().toString().isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, pesan.getText().toString());
            startActivity(Intent.createChooser(intent, "Send to..."));
        } else {
            Toast.makeText(this, "Pesan tidak boleh kosong!",
                    Toast.LENGTH_LONG).show();
        }
    }
}
