package com.example.wahidari.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView tv_Input, tv_Decimal, tv_Biner, tv_Octal, tv_Hexa;
    String strInput= "", strDecimal = "", strBiner ="", strOcta = "", strHexa = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize var
        tv_Input      = findViewById(R.id.tv_Input);
        tv_Decimal    = findViewById(R.id.tv_Decimal);
        tv_Biner      = findViewById(R.id.tv_Biner);
        tv_Octal      = findViewById(R.id.tv_Octal);
        tv_Hexa       = findViewById(R.id.tv_Hexa);

        // initialize savedInstance
        if (savedInstanceState != null){
            tv_Input.setText(savedInstanceState.getString("tv_Input"));
            tv_Decimal.setText(savedInstanceState.getString("tv_Decimal"));
            tv_Biner.setText(savedInstanceState.getString("tv_Biner"));
            tv_Octal.setText(savedInstanceState.getString("tv_Octal"));
            tv_Hexa.setText(savedInstanceState.getString("tv_Hexa"));
            strInput = tv_Input.getText().toString();
        }
    }

    // Show Notification Using Toast
    public void showNotification(String Message)   {
        Toast.makeText(this, Message, Toast.LENGTH_SHORT).show();
    }

    // Clear All Input or Variabel
    private void clearAll(){
        strInput   = "";
        strDecimal = "";
        strBiner   = "";
        strOcta   = "";
        strHexa    = "";
        tv_Input.setText("");
        tv_Decimal.setText("");
        tv_Biner.setText("");
        tv_Octal.setText("");
        tv_Hexa.setText("");
        showNotification("Clear All");
    }

    // Insert Num
    private void insertNum(char n) {
        strInput = strInput + String.valueOf(n);
        tv_Input.setText(strInput);
    }

    // Convert From Decimal to Biner
    public static String decToBiner(String s) {
        long x = Long.parseLong(s);
        String print = "";
        int loop = 0;
        int temp[] = new int[999999];
        while (true) {
            temp[loop] = (int) (x % 2);
            loop++;
            x = x / 2;
            if (x == 0) {
                break;
            }
        }
        for (int i = 0; i < loop; i++) {
            print = print + String.valueOf(temp[loop - (i + 1)]);
        }
        return print;
    }

    // Convert From Decimal to Octa
    public static String decToOcta(String s) {
        long x = (long) Integer.decode(s);
        String print = "";
        int loop = 0;
        int temp[] = new int[999999];
        while (true) {
            temp[loop] = (int) (x % 8);
            loop++;
            x = x / 8;
            if (x == 0) {
                break;
            }
        }
        for (int i = 0; i < loop; i++) {
            print = print + String.valueOf(temp[loop - (i + 1)]);
        }
        return print;
    }

    // Convert From Decimal to Hexa
    public static String decToHexa(String s) {
        long x = (long) Integer.decode(s);
        String print = "";
        int loop = 0;
        String temp[] = new String[999999];
        while (true) {
            temp[loop] = String.valueOf(x % 16);
            loop++;
            x = x / 16;
            if (x == 0) {
                break;
            }
        }
        for (int i = 0; i < loop; i++) {
            if (temp[i].equals("10")) {
                temp[i] = "A";
            }
            if (temp[i].equals("11")) {
                temp[i] = "B";
            }
            if (temp[i].equals("12")) {
                temp[i] = "C";
            }
            if (temp[i].equals("13")) {
                temp[i] = "D";
            }
            if (temp[i].equals("14")) {
                temp[i] = "E";
            }
            if (temp[i].equals("15")) {
                temp[i] = "F";
            }

        }
        for (int i = 0; i < loop; i++) {
            print = print + String.valueOf(temp[loop - (i + 1)]);
        }
        return print;
    }

    // Convert From Decimal to Biner Using Built In Method
    public static String DecToBiner(String n){
        String print = "";
        print = String.valueOf(Integer.toBinaryString(Integer.valueOf(n)));
        return print;
    }

    // Convert From Decimal to Octa Using Built In Method
    public static String DecToOcta(String n){
        String print = "";
        print = String.valueOf(Integer.toOctalString(Integer.valueOf(n)));
        return print;
    }

    // Convert From Decimal to Hexa Using Built In Method
    public static String DecToHexa(String n){
        String print = "";
        print = String.valueOf(Integer.toHexString(Integer.valueOf(n)));
        return print;
    }

    // Convert From Input to Dec First
    public static String inputToDec(String s) {
        char[] x = s.toCharArray();
        long pars[] = new long[x.length];
        long temp = 0;
        for (int i = 0; i < x.length; i++) {
            if (x[i] == '0') {
                pars[x.length - (i + 1)] = 0;
            }
            if (x[i] == '1') {
                pars[x.length - (i + 1)] = 1;
            }
            if (x[i] == '2') {
                pars[x.length - (i + 1)] = 2;
            }
            if (x[i] == '3') {
                pars[x.length - (i + 1)] = 3;
            }
            if (x[i] == '4') {
                pars[x.length - (i + 1)] = 4;
            }
            if (x[i] == '5') {
                pars[x.length - (i + 1)] = 5;
            }
            if (x[i] == '6') {
                pars[x.length - (i + 1)] = 6;
            }
            if (x[i] == '7') {
                pars[x.length - (i + 1)] = 7;
            }
            if (x[i] == '8') {
                pars[x.length - (i + 1)] = 8;
            }
        }
        for (int i = 0; i < pars.length; i++) {
            temp = (long) (temp + (pars[i] * (Math.pow(9, i))));
        }
        System.out.println(temp);
        return String.valueOf(temp);
    }

    // Do Convert
    private void convert () {
        if (strInput != ""){
            strDecimal = inputToDec(strInput);
            strBiner   = DecToBiner(strDecimal);
            strOcta    = DecToOcta(strDecimal);
            strHexa    = DecToHexa(strDecimal);
            tv_Decimal.setText(strDecimal);
            tv_Biner.setText(strBiner);
            tv_Octal.setText(strOcta);
            tv_Hexa.setText(strHexa);
        }
        else {
            showNotification("Please Enter Input!");
        }
    }

    // Handle Each Button is Clicked
    public void btn_Clicked(View view) {
        switch (view.getId()) {
            case R.id.btn_zero:
                insertNum('0');
                break;
            case R.id.btn_one:
                insertNum('1');
                break;
            case R.id.btn_two:
                insertNum('2');
                break;
            case R.id.btn_three:
                insertNum('3');
                break;
            case R.id.btn_four:
                insertNum('4');
                break;
            case R.id.btn_five:
                insertNum('5');
                break;
            case R.id.btn_six:
                insertNum('6');
                break;
            case R.id.btn_seven:
                insertNum('7');
                break;
            case R.id.btn_eight:
                insertNum('8');
                break;
            case R.id.btn_Convert:
                convert();
                break;
            case R.id.btn_Clear:
                clearAll();
                break;
        }
    }

    // Handle On Pause | Screen Rotated
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("tv_Input",   (String) tv_Input.getText());
        savedInstanceState.putString("tv_Decimal", (String) tv_Decimal.getText());
        savedInstanceState.putString("tv_Biner",   (String) tv_Biner.getText());
        savedInstanceState.putString("tv_Octal",   (String) tv_Octal.getText());
        savedInstanceState.putString("tv_Hexa",    (String) tv_Hexa.getText());
    }
}
