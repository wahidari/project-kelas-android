# Project Kelas Pemrograman Perangkat Bergerak (Android) 2018

[![](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://wahidari.gitlab.io)
[![](https://semaphoreci.com/api/v1/projects/2f1a5809-418b-4cc2-a1f4-819607579fe7/400484/shields_badge.svg)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/docs-latest-brightgreen.svg?style=flat&maxAge=86400)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/Find%20Me-%40wahidari-009688.svg?style=social)](https://wahidari.gitlab.io)

## Language

- [![](https://img.shields.io/badge/java-8-red.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/xml-1.0-green.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/kotlin-1.2-9C27B0.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/firebase-16.0.1-yellow.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/sqlite-3-blue.svg)](https://wahidari.gitlab.io) 

## Screenshot

- ##### Week 4

    ![](./Week4/ss/a.png)
    ![](./Week4/ss/b.png)
    
-----------------------------------------------
    
- ##### Week 5

    ![](./Week5/ss/a.png)
    ![](./Week5/ss/b.png)

-----------------------------------------------

- ##### Week 6

    ![](./Week6/ss/a.png)
    
-----------------------------------------------
    
- ##### Week 7

    ![](./Week7/ss/a.png)
    
    ![](./Week7/ss/b.png)
    
-----------------------------------------------
    
- ##### UTS

    ![](./uts/ss/a.png)
    
    ![](./uts/ss/b.png)
    
-----------------------------------------------

- #### Data

    ![](./Data/ss/a.jpg)
    
    ![](./Data/ss/b.jpg)
    
    ![](./Data/ss/c.jpg)
    
    ![](./Data/ss/d.jpg)
    
    ![](./Data/ss/e.jpg)
    
    ![](./Data/ss/f.jpg)
    
    ![](./Data/ss/g.jpg)
    
    ![](./Data/ss/h.jpg)
    
-----------------------------------------------

- #### Kuis

    ![](./Kuis/ss/a.jpg)
    
    ![](./Kuis/ss/b.jpg)
    
    ![](./Kuis/ss/c.jpg)
    
-----------------------------------------------

- #### MusicPlayer

    ![](./MusicPlayer/ss/a.jpg)
    
    ![](./MusicPlayer/ss/b.jpg)
    
-----------------------------------------------

- #### Firebase

    ![](./firebase/ss/a.jpg)
    
    ![](./firebase/ss/b.jpg)
    
## License
> This program is Free Software: 
You can use, study, share and improve it at your will. 
Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.