package com.example.wahidari.kotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val tvData1 = findViewById(R.id.textView3) as TextView
        tvData1.setText(getIntent().getStringExtra("data1"))
    }
}
