package com.example.wahidari.kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun SendIntent(view: View) {
        val pesan = findViewById(R.id.editText2) as EditText
        if (!pesan.getText().toString().isEmpty())
        {
            val intent = Intent(this, Main2Activity::class.java)
            intent.putExtra("data1", pesan.getText().toString())
            startActivity(intent)
        }
        else
        {
            Toast.makeText(this, "Pesan tidak boleh kosong!",
                    Toast.LENGTH_LONG).show()
        }
    }
    fun SendSms(view:View) {
        val pesan = findViewById(R.id.editText2) as EditText
        if (!pesan.getText().toString().isEmpty())
        {
            val intent = Intent(Intent.ACTION_SEND)
            intent.setType("text/plain")
            intent.putExtra(Intent.EXTRA_TEXT, pesan.getText().toString())
            startActivity(Intent.createChooser(intent, "Send to..."))
        }
        else
        {
            Toast.makeText(this, "Pesan tidak boleh kosong!",
                    Toast.LENGTH_LONG).show()
        }
    }

}
