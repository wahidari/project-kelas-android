package com.example.wahidari.firebase;

public class Agenda {
    public String id, item;

    public Agenda(){

    }

    public Agenda(String id, String item){
        this.id = id;
        this.item = item;
    }

    public String getId(){
        return id;
    }

    public String getItem(){
        return item;
    }
}
