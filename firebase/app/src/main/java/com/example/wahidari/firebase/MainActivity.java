package com.example.wahidari.firebase;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mFirebaseReference;

    EditText editAgenda;
    Button buttonSimpan;
    ListView listAgenda;
    List<Agenda> agendas;

    public static final String AGENDA_ID = "com.example.wahidari.firebase.agendaId";
    public static final String AGENDA_ITEM = "com.example.wahidari.firebase.agendaItem";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editAgenda   = findViewById(R.id.tv_agenda);
        buttonSimpan = findViewById(R.id.btn_simpan);
        listAgenda   = findViewById(R.id.listAgenda);

        agendas = new ArrayList<>();

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseReference = mFirebaseDatabase.getReference();


        listAgenda.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Agenda agenda = agendas.get(position);
                showUpdateDeleteDialog(agenda.getId(), agenda.getItem());
            }
        });

        buttonSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = editAgenda.getText().toString();
                String id = mFirebaseReference.push().getKey();

                Agenda agenda = new Agenda(id, item);

                mFirebaseReference.child(id).setValue(agenda);
                Toast.makeText(getApplicationContext(), "Agenda ditambahkan", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean updateAgenda(String id, String item) {
        Agenda agenda = new Agenda(id, item);
        mFirebaseReference.child(id).setValue(agenda);
        Toast.makeText(getApplicationContext(), "Agenda terupdate", Toast.LENGTH_LONG).show();
        return true;
    }
    private boolean deleteAgenda(String id) {
        mFirebaseReference.child(id).removeValue();
        Toast.makeText(getApplicationContext(),"Agenda telah dihapus", Toast.LENGTH_LONG).show();
        return true;
    }

    private void showUpdateDeleteDialog(final String id, String item){
        final EditText editText;
        final Button buttonUpdate;
        final Button buttonDelete;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.agenda_update, null);
        dialogBuilder.setView(dialogView);

        editText = (EditText) dialogView.findViewById(R.id.editAgenda);
        buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdate);
        buttonDelete = (Button) dialogView.findViewById(R.id.buttonDelete);

        dialogBuilder.setTitle(item);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = editText.getText().toString().trim();
                if (!TextUtils.isEmpty(item)){
                    updateAgenda(id, item);
                    b.dismiss();
                }
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAgenda(id);
                b.dismiss();
            }
        });

    }

    @Override
    protected void onStart(){
        super.onStart();

        mFirebaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                agendas.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Agenda agenda = postSnapshot.getValue(Agenda.class);
                    agendas.add(agenda);
                }

                AgendaList agendaAdapter = new AgendaList(MainActivity.this, agendas);
                listAgenda.setAdapter(agendaAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void simpan(View view) {
        String item = editAgenda.getText().toString();
        String id = mFirebaseReference.push().getKey();

        Agenda agenda = new Agenda(id, item);

        mFirebaseReference.child(id).setValue(agenda);
        Toast.makeText(this, "Agenda ditambahkan", Toast.LENGTH_LONG).show();

    }
}
