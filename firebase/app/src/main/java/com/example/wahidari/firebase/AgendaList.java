package com.example.wahidari.firebase;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AgendaList extends ArrayAdapter<Agenda> {
    private Activity context;
    List<Agenda> agendas;

    public AgendaList(Activity context, List<Agenda> agendas){
        super(context, R.layout.agenda_list, agendas);
        this.context = context;
        this.agendas = agendas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View listAgenda = inflater.inflate(R.layout.agenda_list, null, true);

        TextView isiAgenda = listAgenda.findViewById(R.id.teksItem);

        Agenda agenda = agendas.get(position);
        isiAgenda.setText(agenda.getItem());


        return listAgenda;
    }
}
