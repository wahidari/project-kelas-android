package com.example.wahidari.musicplayer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
/**
 * Created by @wahidari on 13/06/18.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton buttonStart;
    private ImageButton buttonStop;
    boolean isPlay = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStart = findViewById(R.id.buttonStart);
        buttonStop = findViewById(R.id.buttonStop);

        buttonStart.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == buttonStart && !isPlay) {
            // Start Service
            startService(new Intent(this, MyService.class));
            isPlay = true;
        } else if (view == buttonStop) {
            // Stop Service
            stopService(new Intent(this, MyService.class));
            isPlay = false;
        }
    }
}
