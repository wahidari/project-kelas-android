package com.example.wahidari.musicplayer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
/**
 * Created by @wahidari on 13/06/18.
 */

public class MyService extends Service {

    private MediaPlayer player;
    public static final int NOTIFICATION_ID = 5453;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Set Musik Default Berdasarkan Ringtone
        player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);

        // Set Musik agar terus berjalan
        player.setLooping(true);

        // Jalankan Musik
        player.start();

        // Tampilkan Notifikasi Bahwa Music Player Dijalankan
        showNotif();

        //we have some options for service
        //start sticky means service will be explicity started and stopped
        return START_STICKY;

    }

    public void showNotif() {
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.music)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setContentText("Music Player Service Is Started")
                .build();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop Musik Saat Aplikasi Destroyed
        player.stop();
    }
}
