package com.example.wahidari.quiz;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FormActivity extends AppCompatActivity {

    EditText formNama, formNip, formAlamat, formTelponA, formTelponB, formHp;
    Button Simpan, Bersih;
    Spinner spinnerKelamin;

    //Data for populating in Spinner
    String [] array_kelamin={"Laki-Laki","Perempuan"};
    String nama, nip, kelamin, tanggal, alamat, telpon, hp;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TextView tvDateResult;
    private ImageButton btDatePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        //Referring the Views
        formNama        = findViewById(R.id.inputNama);
        formNip         = findViewById(R.id.inputNip);
        spinnerKelamin  = findViewById(R.id.inputKelamin);
        formAlamat      = findViewById(R.id.inputAlamat);
        formTelponA     = findViewById(R.id.inputTelponA);
        formTelponB     = findViewById(R.id.inputTelponB);
        formHp          = findViewById(R.id.inputHp);

        Simpan = findViewById(R.id.btnSimpan);
        Bersih = findViewById(R.id.btnBersih);

        //Creating Adapter for Spinner for adapting the data from array to Spinner
        ArrayAdapter adapter= new ArrayAdapter(FormActivity.this,android.R.layout.simple_spinner_item,array_kelamin);
        spinnerKelamin.setAdapter(adapter);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tvDateResult = findViewById(R.id.textView7);
        btDatePicker = findViewById(R.id.bt_datepicker);
        btDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        //Creating Listener for Button
        Simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Getting the Values from Views(Edittext & Spinner)
                if (!formNama.getText().toString().toString().isEmpty() && !formNip.getText().toString().toString().isEmpty() && !formAlamat.getText().toString().toString().isEmpty() && !formTelponA.getText().toString().toString().isEmpty() && !formTelponB.getText().toString().toString().isEmpty() && !formHp.getText().toString().toString().isEmpty() && !tvDateResult.getText().toString().toString().isEmpty()){
                    nama  = formNama.getText().toString();
                    nip   = formNip.getText().toString();
                    kelamin  = spinnerKelamin.getSelectedItem().toString();
                    tanggal = tvDateResult.getText().toString();
                    alamat = formAlamat.getText().toString();
                    telpon  = formTelponA.getText().toString() + formTelponB.getText().toString();
                    hp = formHp.getText().toString();

                    //Intent For Navigating to Second Activity
                    Intent i = new Intent(FormActivity.this,DataActivity.class);

                    //For Passing the Values to Second Activity
                    i.putExtra("nama" , nama);
                    i.putExtra("nip"  , nip);
                    i.putExtra("kelamin" , kelamin);
                    i.putExtra("tanggal", tanggal);
                    i.putExtra("alamat", alamat);
                    i.putExtra("telpon" , telpon);
                    i.putExtra("hp" ,hp);
                    startActivity(i);
                }
                else {
                    Toast.makeText(getApplicationContext(), "All Field Must Be Filled", Toast.LENGTH_SHORT).show();
                }

            }
        });

        Bersih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formNama.setText("");
                formNip.setText("");
                tvDateResult.setText("");
                formAlamat.setText("");
                formTelponA.setText("");
                formTelponB.setText("");
                formHp.setText("");
            }
        });
    }

    private void showDateDialog() {

        /**
         * Calendar untuk mendapatkan tanggal sekarang
         */
        Calendar newCalendar = Calendar.getInstance();

        /**
         * Initiate DatePicker dialog
         */
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                /**
                 * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                 */

                /**
                 * Set Calendar untuk menampung tanggal yang dipilih
                 */
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                /**
                 * Update TextView dengan tanggal yang kita pilih
                 */
                tvDateResult.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        /**
         * Tampilkan DatePicker dialog
         */
        datePickerDialog.show();
    }
}
