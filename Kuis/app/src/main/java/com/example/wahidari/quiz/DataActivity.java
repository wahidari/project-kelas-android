package com.example.wahidari.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DataActivity extends AppCompatActivity {
    TextView t1,t2,t3,t4,t5,t6, t7;
    String nama, nip, kelamin, tanggal, alamat, telpon, hp;
    Button Kembali;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        t1= (TextView) findViewById(R.id.dataNama);
        t2= (TextView) findViewById(R.id.dataNip);
        t3= (TextView) findViewById(R.id.dataKelamin);
        t4= (TextView) findViewById(R.id.dataTanggal);
        t5= (TextView) findViewById(R.id.dataAlamat);
        t6= (TextView) findViewById(R.id.dataTelpon);
        t7= (TextView) findViewById(R.id.dataHp);

        Kembali = findViewById(R.id.btnKembali);

        //Getting the Intent
        Intent i = getIntent();

        //Getting the Values from First Activity using the Intent received
        nama =i.getStringExtra("nama");
        nip  =i.getStringExtra("nip");
        kelamin =i.getStringExtra("kelamin");
        tanggal=i.getStringExtra("tanggal");
        alamat=i.getStringExtra("alamat");
        telpon =i.getStringExtra("telpon");
        hp =i.getStringExtra("hp");

        //Set the Values to TextView
        t1.setText(nama);
        t2.setText(nip);
        t3.setText(kelamin);
        t4.setText(tanggal);
        t5.setText(alamat);
        t6.setText(telpon);
        t7.setText(hp);

        Kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent For Navigating to Second Activity
                Intent i = new Intent(DataActivity.this, FormActivity.class);
                startActivity(i);
            }
        });
    }
}
