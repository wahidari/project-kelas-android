package com.example.wahidari.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView tv_result, tv_numA, tv_numB, tv_operand, tv_equal;
    Double numA, numB, result;
    String strNumA = "", strNumB ="", operand = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize var
        tv_numA    = findViewById(R.id.tv_numA);
        tv_numB    = findViewById(R.id.tv_numB);
        tv_operand = findViewById(R.id.tv_operand);
        tv_result  = findViewById(R.id.tv_result);
        tv_equal   = findViewById(R.id.tv_equal);


        // initialize savedInstance
        if (savedInstanceState != null){
            tv_numA.setText(savedInstanceState.getString("tv_numA"));
            tv_numB.setText(savedInstanceState.getString("tv_numB"));
            tv_operand.setText(savedInstanceState.getString("tv_operand"));
            tv_result.setText(savedInstanceState.getString("tv_result"));
            tv_equal.setText(savedInstanceState.getString("tv_equal"));
            strNumA = tv_numA.getText().toString();
            strNumB = tv_numB.getText().toString();
            operand = tv_operand.getText().toString();
        }
    }

    // show notification using Toast
    public void showNotification(String Message)   {
        Toast.makeText(this, Message, Toast.LENGTH_SHORT).show();
    }

    // insert Num
    private void insertNum(char n) {
        if (operand == ""){
            if (n == '.'){
                if (strNumA.charAt(strNumA.length() - 1) != '.') {
                    strNumA = strNumA + String.valueOf(n);
                    tv_numA.setText(strNumA);
                }
            }else {
                strNumA = strNumA + String.valueOf(n);
                tv_numA.setText(strNumA);
            }
        }else {
            if (n == '.'){
                if (strNumB.charAt(strNumB.length() - 1) != '.') {
                    strNumB = strNumB + String.valueOf(n);
                    tv_numB.setText(strNumB);
                }
            }else {
                strNumB = strNumB + String.valueOf(n);
                tv_numB.setText(strNumB);
            }
        }
    }

    // insert Opr
    private void insertOperand(String opr) {
        operand = opr;
        tv_operand.setText(operand);
    }

    // Calculate
    private void calculate() {
        if (strNumA != "" && strNumB != "" && operand != "") {
            try {
                numA = Double.parseDouble(strNumA);
                numB = Double.parseDouble(strNumB);
                if(operand == "+"){
                    result = numA + numB;
                }else if(operand == "-"){
                    result = numA - numB;
                }else if(operand == "/"){
                    result = numA / numB;
                }else if(operand == "x") {
                    result = numA * numB;
                }
                tv_equal.setText("=");
                tv_result.setText(""+result);
            }catch (Exception e) {
                showNotification("Wrong input. Try again.");
            }
        }else {
            showNotification("Input The Number");
        }
    }

    private void clearAll(){
        operand = "";
        numA = 0.0;
        numB = 0.0;
        result = 0.0;
        strNumA = "";
        strNumB="";
        tv_numA.setText("");
        tv_numB.setText("");
        tv_operand.setText("");
        tv_result.setText("");
        tv_equal.setText("");
    }

    // handle each button is clicked
    public void btn_clicked(View view) {
        switch (view.getId()) {
            case R.id.btn_zero:
                insertNum('0');
                break;
            case R.id.btn_one:
                insertNum('1');
                break;
            case R.id.btn_two:
                insertNum('2');
                break;
            case R.id.btn_three:
                insertNum('3');
                break;
            case R.id.btn_four:
                insertNum('4');
                break;
            case R.id.btn_five:
                insertNum('5');
                break;
            case R.id.btn_six:
                insertNum('6');
                break;
            case R.id.btn_seven:
                insertNum('7');
                break;
            case R.id.btn_eight:
                insertNum('8');
                break;
            case R.id.btn_nine:
                insertNum('9');
                break;
            case R.id.btn_point:
                if (operand == ""){
                    if (tv_numA.getText().toString() != ""){
                        insertNum('.');
                    }
                } else {
                    if (tv_numB.getText().toString() != ""){
                        insertNum('.');
                    }
                }
                break;
            case R.id.btn_add:
                if (tv_numA.getText().toString() != ""){
                    insertOperand("+");
                }else{
                    showNotification("Input The Number");
                }
                break;
            case R.id.btn_minus:
                if (tv_numA.getText().toString() != ""){
                    insertOperand("-");
                }else{
                    showNotification("Input The Number");
                }
                break;
            case R.id.btn_times:
                if (tv_numA.getText().toString() != ""){
                    insertOperand("x");
                }else{
                    showNotification("Input The Number");
                }
                break;
            case R.id.btn_divide:
                if (tv_numA.getText().toString() != ""){
                    insertOperand("/");
                }else{
                    showNotification("Input The Number");
                }
                break;
            case R.id.btn_equal:
                calculate();
                break;
            case R.id.btn_clear:
                clearAll();
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("tv_numA", (String) tv_numA.getText());
        savedInstanceState.putString("tv_numB", (String) tv_numB.getText());
        savedInstanceState.putString("tv_operand", (String) tv_operand.getText());
        savedInstanceState.putString("tv_result", (String) tv_result.getText());
        savedInstanceState.putString("tv_equal", (String) tv_equal.getText());
    }
}
